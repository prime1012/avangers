package com.app.avangers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import java.text.SimpleDateFormat
import java.util.*


class ListViewAdapter(private val callback: ((name: String) -> Unit)? = null) : BaseAdapter() {

    private var mContext: Context? = null
    private val arraylist: ArrayList<AvangerModel> = arrayListOf()
    private var fullList: ArrayList<AvangerModel> = arrayListOf()

    fun setList(list: ArrayList<AvangerModel>, context: Context) {
        fullList = list
        mContext = context
        this.arraylist.addAll(fullList)
        notifyDataSetChanged()
    }

    inner class ViewHolder {
        internal var name: TextView? = null
        internal var time: TextView? = null
        internal var image: ImageView? = null
    }

    override fun getCount(): Int {
        return fullList.size
    }

    override fun getItem(position: Int): AvangerModel {
        return fullList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        var view = view
        val holder: ViewHolder
        if (view == null) {
            holder = ViewHolder()
            view = LayoutInflater.from(mContext).inflate(R.layout.lv_item, null)
            holder.name = view!!.findViewById(R.id.name) as TextView
            holder.time = view.findViewById(R.id.time) as TextView
            holder.image = view.findViewById(R.id.image) as ImageView
            view.tag = holder
        } else {
            holder = view.tag as ViewHolder
        }

        holder.name?.text = fullList[position].title
        holder.time?.text = convertTime()
        holder.image?.setImageResource(fullList[position].image)

        view.setOnClickListener {
            callback?.invoke(fullList[position].title)
        }

        return view
    }

    fun filter(charText: String) {
        var charText = charText
        charText = charText.toLowerCase(Locale.getDefault())
        fullList.clear()
        if (charText.isEmpty()) {
            fullList.addAll(arraylist)
        } else {
            for (wp in arraylist) {
                if (wp.title.toLowerCase(Locale.getDefault()).contains(charText)) {
                    fullList.add(wp)
                }
            }
        }
        notifyDataSetChanged()
    }

    fun convertTime() : String{
        val formatter = SimpleDateFormat("dd-MMM-yyyy HH:mm")
        return formatter.format(Date(System.currentTimeMillis()))
    }

}

