package com.app.avangers

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import kotlinx.android.synthetic.main.main_fragment.*
import android.R.id
import android.widget.EditText




class FragmentMain : BaseFragment() {

    private lateinit var adapter: ListViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val toolbar = mainActivity.findViewById(R.id.toolbar) as Toolbar
        mainActivity.setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp)
        toolbar.setNavigationOnClickListener {
            mainActivity.openDrawer()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        println(getString(R.string.about, getString(R.string.iron_1)))

        adapter = ListViewAdapter{ Toast.makeText(context, it, Toast.LENGTH_SHORT).show() }
        listview.adapter = adapter
        adapter.setList(MainActivity.list, view.context)

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val myActionMenuItem = menu.findItem(R.id.action_search)
        val searchView = myActionMenuItem.actionView as androidx.appcompat.widget.SearchView
        searchView.maxWidth = Integer.MAX_VALUE
        searchView.queryHint = "Search"
        searchView.setIconifiedByDefault(true)

        searchView.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                adapter.filter(s)
                return false
            }
        })
        super.onCreateOptionsMenu(menu, inflater)
    }

//    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        inflater.inflate(R.menu.search_menu, menu)
//
////        val searchItem = menu.findItem(R.id.action_search)
////        val searchManager = activity!!.getSystemService(SEARCH_SERVICE) as SearchManager
////
////        if (searchItem != null) {
////            searchView = searchItem.actionView as SearchView
////        }
////        if (searchView != null) {
////            searchView.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
////            searchView.setOnQueryTextListener(queryTextListener)
////        }
//        super.onCreateOptionsMenu(menu, inflater)
//    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                goBack()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }
}