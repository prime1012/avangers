package com.app.avangers

import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import java.util.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, DrawerLocker {

    private var doubleBackToExitPressedOnce: Boolean = false

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        setSupportActionBar(toolbar)
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        supportActionBar?.setDisplayShowTitleEnabled(false)

//        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp)
//        toolbar.setNavigationOnClickListener { openDrawer() }

        list = arrayListOf()
        list.add(AvangerModel(R.drawable.capitan, getString(R.string.cap_1), getString(R.string.cap_2), getString(R.string.cap_3)))
        list.add(AvangerModel(R.drawable.hulk, getString(R.string.hulk_1), getString(R.string.hulk_2), getString(R.string.hulk_3)))
        list.add(AvangerModel(R.drawable.iron, getString(R.string.iron_1), getString(R.string.iron_2), getString(R.string.iron_3)))
        list.add(AvangerModel(R.drawable.spider, getString(R.string.spider_1), getString(R.string.spider_2), getString(R.string.spider_3)))
        list.add(AvangerModel(R.drawable.thor, getString(R.string.thor_1), getString(R.string.thor_2), getString(R.string.thor_3)))
        list.add(AvangerModel(R.drawable.widow, getString(R.string.widow_1), getString(R.string.widow_2), getString(R.string.widow_3)))

        if (savedInstanceState == null){
            hardReplaceFragment(FragmentMain())
        }

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun setDrawerLocked(shouldLock: Boolean) {
        if (shouldLock) {
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        } else {
            drawer_layout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }
    }

    fun openDrawer() { drawer_layout?.openDrawer(GravityCompat.START) }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (supportFragmentManager.backStackEntryCount != 0) {
                super.onBackPressed()
                return
            }

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed()
                finish()
                return
            }

            this.doubleBackToExitPressedOnce = true
            Toast.makeText(this, "To exit press BACK again", Toast.LENGTH_SHORT).show()

            Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
        }
    }

    fun hardReplaceFragment(fragment: Fragment) {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(R.id.container, fragment)
            .commitAllowingStateLoss()
    }

    companion object {
        var list = ArrayList<AvangerModel>()
    }
}