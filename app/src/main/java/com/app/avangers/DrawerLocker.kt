package com.app.avangers

interface DrawerLocker {
    fun setDrawerLocked(shouldLock: Boolean)
}